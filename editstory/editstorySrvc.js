'use strict';

app.factory('EditStory', ['FIREBASE_URL','$firebase','$location',function (FIREBASE_URL, $firebase,$location) {
    var ref = new Firebase(FIREBASE_URL);


    var EditStory = {
        getstories: function(storyid){
            return  $firebase(ref.child('stories').child(storyid)).$asObject();
        },
        getauthor: function(storyid){
            var storobj = $firebase(ref.child('stories').child(storyid)).$asObject();
             storobj.$loaded().then(function(stordata){
                console.log('the story data ', stordata.creatorId);
return stordata

            })
        },
        purgestory: function(userid,storyid,storyinfo){
            console.log('uerid ', userid);
            $firebase(ref.child('user_stories').child(userid).child(storyinfo.userkey)).$remove()
             .then(function(stkey){
                    $firebase(ref.child('stories').child(storyid)).$remove()
                    .then(function(uskey) {
                    $location.path('/profile/'+userid);
                });
            })

        }
    };

    return EditStory;

}]);