'use strict';

app.controller('AddStoryCtrl', ['$scope','AddStory','Auth',function ($scope,AddStory,Auth) {
  //get the current logged in user details
    Auth.actuser();
    $scope.newstorycreator = Auth.user;

    //current story input from user
    $scope.currentstory = {};

    //function to add a story
    $scope.addnewstory = function(storyinfo, userinfo){
        //AddStory.gmaplocation(storyinfo);
        AddStory.create(storyinfo,userinfo).then(function(stos){
        AddStory.gmaplocation(stos,storyinfo)
        })
    };
//different overall experiences.
    $scope.myexps = [
        {name:'Awesome',imgp:'awesome.png'},
        {name:'Horrible',imgp:'horrible.png'},
        {name:'It was OK',imgp:'ok.png'},
        {name:'Not doing that again',imgp:'notagain.png'}
    ];


}]);