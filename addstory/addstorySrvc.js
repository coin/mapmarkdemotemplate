'use strict';

app.factory('AddStory', ['FIREBASE_URL','$firebase','$location','$http',function (FIREBASE_URL, $firebase,$location,$http) {
    var ref = new Firebase(FIREBASE_URL);
    var stories = $firebase(ref.child('stories')).$asArray();


    var AddStory = {
        create: function(storyinfo,userinfo){
            storyinfo.creator = userinfo.profile.username;
            storyinfo.creatorId = userinfo.uid;
            storyinfo.userkey = '';

            return stories.$add(storyinfo).then(function (storydata) {
                var storyid = storydata.key();
                $location.path('/editstory/'+storyid);
                $firebase(ref.child('user_stories').child(userinfo.uid)).$push(storyid)
                    .then(function(uskeyref) {
                        $firebase(ref.child('stories').child(storyid)).$update({userkey: uskeyref.key()})

                    });
                return storyid
});

            //need to add this story id to the user_stories node in firebase
        },
        getstories: function(){

        },
        gmaplocation: function(storyid,address){
            var resa = {};
            $http.get('https://maps.googleapis.com/maps/api/geocode/json?address= '+address.street+',+'+address.city+',+'+address.state+',+'+address.country)
                .success(function (addret) {
                    resa = addret.results[0].geometry.location;
                    console.log('yikes ' ,resa);
                    $firebase(ref.child('stories').child(storyid)).$update({long:resa.lng,lat:resa.lat});
                    return addret;
                });
        }
    };

    return AddStory;

}]);