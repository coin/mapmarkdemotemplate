'use strict';

/**
 * @ngdoc overview
 * @name mapmarkdemo
 * @description
 * # mapmarkdemo
 *
 * Main module of the application.
 */
var app =
    angular.module('mapmarkdemo', [
            'ngAnimate',
            'ngCookies',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch',
            'firebase'
        ])
    .constant('FIREBASE_URL', 'https://mapmarkdemo.firebaseio.com/')

