'use strict';

app.controller('ProfileCtrl', ['Profile','$scope','$routeParams','Auth','$timeout','usstories','$modal',function (Profile,$scope,$routeParams,Auth,$timeout,usstories,$modal) {

    //show user list of stories
    $scope.showulist = true;

    //initialize map markers
    $scope.markers = [];
    //$scope.markob = {};
    $scope.showumap = false;

     $scope.loadmap = function() {
         $scope.showumap = true;
         $scope.showulist = false;

     };
var popmap = function(){
         angular.forEach(usstories, function(value, key) {


            //$scope.markob[key] = {id:key,latitude:value.lat,
            //    longitude:value.long,title:value.title};
            $scope.markers.push({id:key,coords:{latitude:value.lat,longitude:value.long},
                showWindow: true,title:value.title,modstorydata:value,icon:'assets/images/map/'+value.myexp.imgp})


        },console.log('e'));
    };

    $scope.hidemap = function(){
        $scope.showulist = true;
        $scope.showumap = false;
    };


    //Let's set $scope.profile to equal to the current page profile
    $scope.profile = Profile.getuser($routeParams.profid);
    Profile.liststories($routeParams.profid).then(function(mylist){
        console.log('eat it ', mylist);
        //show list of stories
        $scope.mylist = mylist;
        //set markers of list of stories
        $timeout(function(){
            popmap();

        },1200)


    });


    $scope.profStatus = Auth.isloggedIn();
    //check if current user uid is equal to the profile page we are on
    /*
     If Auth.uid === $routeParams.profid,
     set is admin = to true and show edit link
     */
    //$scope.iscreator = $routeParams.profid;
    $scope.curruser = Auth.user;


    console.log('current user ', $scope.curruser);

    $scope.map = { center: { latitude: 45, longitude: -73 }, zoom: 3 };

    //load story details in a modal
    $scope.mylistopen = function (storyinfo) {

        var logmodalinstance = $modal.open({
            templateUrl: 'profile/_storymodal.html',
            controller: 'ProfmodalCtrl',
            size: 'lg',
            resolve: {
                mystorydata: function(){
                    return storyinfo
                }

            }
        });
    }


    }]);