'use strict';

app.factory('Profile',['FIREBASE_URL','$firebase','EditStory','$q', function (FIREBASE_URL, $firebase,EditStory,$q) {
    var ref = new Firebase(FIREBASE_URL);


    var Profile = {
        getuser: function (userId) {
            return $firebase(ref.child('profile/').child(userId)).$asObject();
        },
        liststories: function (userId) {
            var defer = $q.defer();

            $firebase(ref.child('user_stories').child(userId))
                .$asArray()
                .$loaded()
                .then(function (data) {
                    var mystories = {};

                    for (var i = 0; i < data.length; i++) {
                        var value = data[i].$value;
                        console.log('value poolice ', value);
                        mystories[value] = EditStory.getstories(value);


                    }
                    defer.resolve(mystories);
                });

            return defer.promise;
        }

    };
    return Profile;

}]);