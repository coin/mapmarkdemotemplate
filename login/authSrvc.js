'use strict';

app.factory('Auth',['$firebaseAuth','FIREBASE_URL','$firebase', function ($firebaseAuth, FIREBASE_URL,$firebase) {
    var ref = new Firebase(FIREBASE_URL);
    var auth = $firebaseAuth(ref);


    var Auth = {
        register: function(nuser) {
          return  auth.$createUser({email:nuser.email,
                             password:nuser.password}).then(function(authData) {
                console.log('user has been created ',authData);
                return auth.$authWithPassword({
                    email: nuser.email,
                    password: nuser.password
                });
            })

        },
        newprofile: function (user,username) {

                        var profile = {
                            username: username,
                            myuid: user.uid,
                            ownavatar: 'https://www.gravatar.com/avatar/precz'+username+'/?s=140'

                        };
                        var profileRef = $firebase(ref.child('profile'));

                        return profileRef.$set(user.uid, profile);



        },
        actuser: function(){
            return auth.$onAuth(function(user) {
                if (user) {
                    //console.log("Authenticated with uid:", user);
                    angular.copy(user, Auth.user);
                    Auth.user.profile = $firebase(ref.child('profile').child(Auth.user.uid)).$asObject();
                    //Auth.user.level = $firebase(ref.child('level').child(Auth.user.uid)).$asObject();
                } else {
                    console.log("Client unauthenticated.")
                }




            })
        },
        isloggedIn: function(){
           return auth.$getAuth()
        },

        authwrap: function(){
            return auth;
        },
        login: function (user) {
            return auth.$authWithPassword({
                email: user.email,
                password: user.password
            });
               },
        logout: function () {
          return auth.$unauth();
        },

            resetpw: function(email){
            return auth.$resetPassword({
                email: email
            })
        },
        user:{}

    };
    return Auth;

}]);