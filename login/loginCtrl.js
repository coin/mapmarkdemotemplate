'use strict';

app.controller('LoginCtrl', ['Auth','$scope','$location','$window',function (Auth,$scope,$location,$window) {


    //setup default form views
    $scope.usRegister = false;
    $scope.usLogin = true;
    $scope.usReset = false;

    $scope.reguser = {};
    $scope.loguser = {};
    $scope.resuser = {};

    //show Register Form
    $scope.showRegister = function(){
        $scope.usRegister = true;
        $scope.usLogin = false;
        $scope.usReset = false;

    };

    //show Login Form
    $scope.showLogin = function(){
        $scope.usRegister = false;
        $scope.usLogin = true;
        $scope.usReset = false;

    };

    //show Password Reset Form
    $scope.showReset = function(){
        $scope.usRegister = false;
        $scope.usLogin = false;
        $scope.usReset = true;
    };

    //register function
    $scope.register = function(user){
        Auth.register(user).then(function(authData){
            return Auth.newprofile(authData,user.username)

            }).then(function(){
            $location.path('/');
            console.log('profile created with no errors')
        }).catch(function(error) {
            console.error("Error: ", error);
    })


    };

    //login function
    $scope.login = function(user){
        Auth.login(user).then(function(authData){
                console.log('Logged in as user', authData.uid);
            $window.location.href='/';
            }).catch(function(error){
                console.error('Authentication failed:', error);
            })

    };

    //reset password function
    $scope.resetPassword = function(user){
        Auth.resetpw(user.email).then(function() {
            console.log("Password reset email sent successfully!" );
            $location.path('/pwresetsuccess')
        }).catch(function(error) {
            console.error("Error: ", error);
        });
    }


}]);