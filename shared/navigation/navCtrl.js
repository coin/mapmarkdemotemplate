'use strict';

//Let's Create our navigation controller
app.controller('NavCtrl',  ['$scope','Auth','$window',function ($scope,Auth,$window) {
  //This function loads the users profile
    Auth.actuser();
    $scope.navuser = Auth.user;


    $scope.uslogout = function(){
        Auth.logout();
        $window.location.href='/';

    };
    $scope.authStatus = Auth.isloggedIn();

    console.log('auth status ', $scope.authStatus)

}]);
