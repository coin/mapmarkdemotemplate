'use strict';

/**
 * @ngdoc overview
 * @name mapmarkdemo
 * @description
 * # mapmarkdemo
 *
 * Main module of the application.
 */
var app =
    angular.module('mapmarkdemoApp', [
            'ngAnimate',
            'ngCookies',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch',
            'firebase',
            'ui.bootstrap',
            'uiGmapgoogle-maps'
        ])
    .constant('FIREBASE_URL', 'https://mapmarkdemo.firebaseio.com/')
        .config(function(uiGmapGoogleMapApiProvider) {
            uiGmapGoogleMapApiProvider.configure({
                //    key: 'your api key',
                v: '3.17',
                libraries: 'geometry,visualization'
            })
        })
        .config(function ($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'home/_home.html',
                    controller: 'HomeCtrl'
                })
                .when('/login',
                {
                    templateUrl: 'login/_login.html',
                    controller: 'LoginCtrl',
                    resolve: {
                        "currentAuth": ["Auth","$location", function (Auth, $location) {
                            return Auth.authwrap().$waitForAuth().then(function (authuser) {
                                if(authuser !==null){
                                    $location.path('/')
                                }
                            })
                        }]
                    }
                })
                .when('/pwresetsuccess', {
                    templateUrl: 'login/_resetsuccess.html',
                    controller: 'LoginCtrl'

                })
                .when('/profile/:profid',{
                    templateUrl: 'profile/_profile.html',
                    controller: 'ProfileCtrl',
                    resolve: {
                        usstories: ['Profile', '$route', function (Profile, $route) {
                            return Profile.liststories($route.current.params.profid);
                        }]
                    }
                })
                .when('/addstory', {
                    templateUrl: 'addstory/_addstory.html',
                    controller: 'AddStoryCtrl',
                    resolve: {
                        "authReq":["Auth",function(Auth){
                            return Auth.authwrap().$requireAuth();
                        }]
                    }
                })
                .when('/editstory/:storyid',{
                    templateUrl: 'editstory/_editstory.html',
                    controller: 'EditStoryCtrl',
                    resolve: {
                        "currentAuth": ["Auth","EditStory","$route","$location", function(Auth,EditStory,$route,$location) {
                            return Auth.authwrap().$requireAuth().then(function(authuser){
                                EditStory.getstories($route.current.params.storyid).$loaded().then(function(ssdata){
                                    if (authuser.uid != ssdata.creatorId || authuser ===null) {
                                        $location.path("/profile/"+authuser.uid);
                                    }

                                })

                            })

                        }]
                    }
                })
                .otherwise({
                    redirectTo: '/'
                })
        });
app.run(["$rootScope", "$location", function($rootScope, $location) {
    $rootScope.$on("$routeChangeError", function(event, next, previous, error) {
        // We can catch the error thrown when the $requireAuth promise is rejected
        // and redirect the user back to the home page
        if (error === "AUTH_REQUIRED") {
            $location.path("/login");
        }
    });
}]);


